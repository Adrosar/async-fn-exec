import { Seoaf, SeoafNext } from "../source/Seoaf";

if (typeof Seoaf !== "function") {
    throw new Error();
}

if (typeof Seoaf.create !== "function") {
    throw new Error();
}

///////////////////////////////////////////////////////////////

const s: Seoaf = new Seoaf();

if (typeof s !== "object") {
    throw new Error();
}

if (typeof s.use !== "function") {
    throw new Error();
}

if (typeof s.end !== "function") {
    throw new Error();
}

///////////////////////////////////////////////////////////////

function fnA1(next: SeoafNext): void {
    next(2);
}

function fnA2(next: SeoafNext, data: number, [a, b]: Array<number>): void {
    setTimeout(() => {
        next(data + a + b);
    }, 200);
}

function fnA3(next: SeoafNext, data: number, args: Array<any>): void {
    setTimeout(() => {
        next(data + "->" + args[0] + args[1]);
    }, 100);
}

const a = 3;
const b = 4

s.use(fnA1);
s.use(fnA2, [a, b]);
s.use(fnA3, ["A", "B"]);

s.end((data: any) => {
    if (data !== "9->AB") {
        throw new Error();
    }
});

///////////////////////////////////////////////////////////////

Seoaf.create((next: SeoafNext) => {

    setTimeout(() => {
        next("X");
    }, 200);

}).use((next: SeoafNext, data: string) => {

    setTimeout(() => {
        next(data + "Y");
    }, 100);

}).end((data: any) => {

    if (data !== "XY") {
        throw new Error();
    }

})

///////////////////////////////////////////////////////////////

type Callback = {
    (data: any): void
}

class Foo {
    public name: string;

    constructor(name: string) {
        this.name = name;
    }

    public ready(fn: Callback) {
        const ref = this;

        setTimeout(() => {
            fn(ref.name);
        }, 100);
    }
}

const foo: Foo = new Foo("FOO");

Seoaf.create().use(foo.ready.bind(foo)).end((data: any) => {
    if (data !== "FOO") {
        throw new Error();
    }
});

Seoaf.create().use((next: SeoafNext) => {
    foo.ready(next);
}).end((data: any) => {
    if (data !== "FOO") {
        throw new Error();
    }
});