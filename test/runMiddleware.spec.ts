import { runMiddleware, SeoafNext } from "../source/helpers";

if (typeof runMiddleware !== "function") {
    throw new Error();
}

///////////////////////////////////////////////////////////////////

const checklist: Array<string> = [];

function fnA1(next: SeoafNext) {
    setTimeout(() => {
        checklist.push("A1");
        next(1);
    }, 3000);
}

function fnA2(next: SeoafNext, data: number) {
    setTimeout(() => {
        checklist.push("A2");
        next(2 + data);
    }, 1000);
}

function fnB1(next: SeoafNext) {
    setTimeout(() => {
        checklist.push("B1");
        next(3);
    }, 4000);
}

function fnB2(next: SeoafNext, data: number) {
    setTimeout(() => {
        checklist.push("B2");
        next(4 + data);
    }, 2000);
}

const matrix = [
    [fnA1, fnA2],
    [fnB1, fnB2]
]

runMiddleware(matrix, (data: Array<any>) => {

    if (data.join(":") !== "3:7") {
        throw new Error();
    }

    if (checklist.join("-") !== "A1-B1-A2-B2") {
        throw new Error();
    }

});