import { Peoaf, PeoafNext, } from "../source/Peoaf";

if (typeof Peoaf !== "function") {
    throw new Error();
}

if (typeof Peoaf.create !== "function") {
    throw new Error();
}

//////////////////////////////////////////////

const peoaf = new Peoaf();

if (typeof peoaf !== "object") {
    throw new Error();
}

if (typeof peoaf.use !== "function") {
    throw new Error();
}

if (typeof peoaf.end !== "function") {
    throw new Error();
}

//////////////////////////////////////////////

const checklist: Array<string | number> = [];

Peoaf.create((next: PeoafNext) => {
    next(0);
    checklist.push(0);
}).use((next: PeoafNext) => {
    setTimeout(() => {
        checklist.push("y");
        next("A");
    }, 400);
}).use((next: PeoafNext) => {
    setTimeout(() => {
        checklist.push("z");
        next("B");
    }, 600);
}).use((next: PeoafNext) => {
    setTimeout(() => {
        checklist.push("x");
        next("C");
    }, 200);
}).end((dataList: Array<any>) => {

    if (dataList.join("") !== "0ABC") {
        throw new Error();
    }

    if (checklist.join("") !== "0xyz") {
        throw new Error();
    }

});

//////////////////////////////////////////////

const addFn = (next: PeoafNext, a: number, b: number) => {
    setTimeout(() => {
        next(a + b);
    }, ((a + b) * 15) + (a * 10) + (b * 3));
}

const p = Peoaf.create();

const x = 2;
const y = 3;

p.use(addFn, [x, y]);
p.use(addFn, [9, 1]);
p.use(addFn, [10, 5]);

p.end((dataList: Array<any>) => {
    if (dataList.join("-") !== "5-10-15") {
        throw new Error();
    }
});

//////////////////////////////////////////////

Peoaf.create().use((next: PeoafNext) => {
    setTimeout(() => {
        next();
    }, 150);
}).use((next: PeoafNext) => {
    setTimeout(() => {
        next(null);
    }, 200);
}).use((next: PeoafNext) => {
    setTimeout(() => {
        next(undefined);
    }, 100);
}).use((next: PeoafNext) => {
    setTimeout(() => {
        next(NaN);
    }, 0);
}).end((data: Array<any>) => {
    if (typeof data[0] !== "undefined") throw new Error();
    if (data[1] !== null) throw new Error();
    if (data[2] !== undefined) throw new Error();
    if (isNaN(data[0]) === false) throw new Error();
});

//////////////////////////////////////////////

type Callback = {
    (data: any): void
}

class Boo {
    public name: string;

    constructor(name: string) {
        this.name = name;
    }

    public ready(fn: Callback) {
        const ref = this;

        setTimeout(() => {
            fn(ref.name);
        }, 100);
    }
}

const boo: Boo = new Boo("BOO");

Peoaf.create(boo.ready.bind(boo)).use((next: PeoafNext) => {

    boo.ready(next);

}).end((data: Array<any>) => {

    if (data[0] !== "BOO") {
        throw new Error();
    }

    if (data[1] !== "BOO") {
        throw new Error();
    }

});