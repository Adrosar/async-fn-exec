# AsyncFnExec

Moduł `async-fn-exec` zawiera narzędzia _(klasy i funkcje)_, które wspomagają programowanie funkcyjne _(głównie asynchroniczne)_.

Moduł oparty jest na repozytorium [Adrosar/ts-startek-kit](https://bitbucket.org/Adrosar/ts-startek-kit) w wersji **1.4.0**

### Instalacja:

W terminalu wykonaj jedno z poleceń:

 - `npm install bitbucket:Adrosar/async-fn-exec#1.0.2`
 - `npm install https://bitbucket.org/Adrosar/async-fn-exec#1.0.2`


## Klasa `Peoaf`:

```TypeScript
import Peoaf from 'async-fn-exec';
```

Równoległe wykonywanie funkcji asynchronicznych _(wszystkie razem)_.


## Klasa `Seoaf`:

```TypeScript
import Seoaf from 'async-fn-exec';
```

Szeregowe wykonywanie funkcji asynchronicznych _(jedna po drugiej)_.