import { Peoaf, PeoafReady } from "./Peoaf";
import { Seoaf, SeoafMiddlewareFunc } from "./Seoaf";

export { SeoafNext } from "./Seoaf";

export function runMiddleware(middlewareMatrix: Array<Array<SeoafMiddlewareFunc>>, done: PeoafReady) {
    const peoaf = new Peoaf();

    for (const key in middlewareMatrix) {
        if (middlewareMatrix.hasOwnProperty(key)) {

            const middlewareMap: Array<SeoafMiddlewareFunc> = middlewareMatrix[key];
            const seoaf = new Seoaf();

            for (const key in middlewareMap) {
                if (middlewareMap.hasOwnProperty(key)) {
                    const middleware: SeoafMiddlewareFunc = middlewareMap[key];
                    seoaf.use(middleware);
                }
            }

            peoaf.use(seoaf.end.bind(seoaf));
        }
    }

    peoaf.end(done);
}