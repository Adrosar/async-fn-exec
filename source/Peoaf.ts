export type PeoafNext = {
    (data?: any): void;
}

export type PeoafMiddlewareFunc = {
    (next: PeoafNext, ...args: Array<any>): void;
}

export type PeoafReady = {
    (data: Array<any>): void;
}

export type PeoafMiddleware = {
    fn: PeoafMiddlewareFunc;
    args: Array<any>;
    data: any;
    isDone: boolean;
}

export type PeoafMiddlewareMap = {
    [name: number]: PeoafMiddleware
}

export class Peoaf {

    static create(fn?: PeoafMiddlewareFunc): Peoaf {
        return new Peoaf(fn);
    }

    private _read!: PeoafReady;
    private _middlewares: PeoafMiddlewareMap = {};
    private _index: number = 0;

    private _check(): boolean {

        for (const key in this._middlewares) {
            if (this._middlewares.hasOwnProperty(key)) {
                if (this._middlewares[key].isDone === false) {
                    return false;
                }
            }
        }

        return true;
    }

    private _getData(): Array<any> {
        let buffer = [];

        for (const key in this._middlewares) {
            if (this._middlewares.hasOwnProperty(key)) {
                buffer.push(this._middlewares[key].data);
            }
        }

        return buffer;
    }

    constructor(fn?: PeoafMiddlewareFunc) {
        if (typeof fn === "function") {
            this.use(fn);
        }
    }

    public use(fn: PeoafMiddlewareFunc, args?: Array<any>): Peoaf {

        if (!(args instanceof Array)) {
            args = [];
        }

        this._middlewares[this._index] = { fn, args, data: undefined, isDone: false };
        this._index++;

        return this;
    }

    public end(ready: PeoafReady): void {
        this._read = ready;

        for (const key in this._middlewares) {
            if (this._middlewares.hasOwnProperty(key)) {
                const item = this._middlewares[key];

                const next: PeoafNext = (data?: any) => {

                    item.isDone = true;
                    item.data = data;

                    if (this._check() === true) {
                        this._read(this._getData());
                    }
                }

                if (typeof item.fn === "function") {
                    item.fn(next, ...item.args);
                }
            }
        }
    }

}