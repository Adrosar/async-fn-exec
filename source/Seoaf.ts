export type SeoafNext = {
    (data?: any): void;
}

export type SeoafReady = {
    (data: any): void;
}

export type SeoafMiddlewareFunc = {
    (next: SeoafNext, data: any, args: Array<any>): void;
}

export type SeoafMiddleware = {
    fn: SeoafMiddlewareFunc;
    args: Array<any>;
    data: any;
    isDone: boolean;
}

export type SeoafMiddlewareMap = {
    [name: number]: SeoafMiddleware
}

export class Seoaf {

    static ITEM = {
        current: 10,
        prev: 20
    }

    static create(fn?: SeoafMiddlewareFunc, args?: Array<any>): Seoaf {
        return new Seoaf(fn, args);
    }

    private _ready!: SeoafReady;
    private _middlewars: SeoafMiddlewareMap = {};
    private _index: number = 0;
    private _length: number = 0;

    private _getItem(mode: number = Seoaf.ITEM.current): SeoafMiddleware {
        if (mode === Seoaf.ITEM.prev) {
            return this._middlewars[this._index - 1];
        }

        return this._middlewars[this._index];
    }

    private _getPrevData(): any {
        const item = this._getItem(Seoaf.ITEM.prev);

        if (typeof item === "object") {
            return item.data;
        }

        return undefined;
    }

    private _getArgs(): Array<any> {
        return this._middlewars[this._index].args;
    }

    private _setDataAndDone(data?: any): void {
        this._getItem().data = data;
        this._getItem().isDone = true;
    }

    private _next(data?: any): void {
        this._setDataAndDone(data);
        this._index++;

        if (this._index < this._length) {
            this._run();
        } else {
            this._ready(this._getPrevData());
        }
    }

    private _run(): void {
        this._getItem().fn(this._next.bind(this), this._getPrevData(), this._getArgs());
    }

    constructor(fn?: SeoafMiddlewareFunc, args?: Array<any>) {
        if (typeof fn === "function") {
            this.use(fn, args);
        }
    }

    public use(fn: SeoafMiddlewareFunc, args?: Array<any>): Seoaf {
        if (!(args instanceof Array)) {
            args = [];
        }

        this._middlewars[this._index] = { fn, args, data: undefined, isDone: false }
        this._index++;

        return this;
    }

    public end(fn: SeoafReady): Seoaf {
        this._ready = fn;
        this._length = this._index;
        this._index = 0;
        this._run();
        return this;
    }
}