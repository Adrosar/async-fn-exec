import { Peoaf, PeoafReady } from "./Peoaf";
import { Seoaf, SeoafMiddlewareFunc } from "./Seoaf";

export { SeoafNext } from "./Seoaf";

export type MiddlewareMatrixData = Array<Array<SeoafMiddlewareFunc>>;

export class MiddlewareMatrix {

    static create(middlewareMatrix: MiddlewareMatrixData): MiddlewareMatrix {
        return new MiddlewareMatrix(middlewareMatrix);
    }

    private _data: MiddlewareMatrixData = [];

    private _run(middlewareMatrix: MiddlewareMatrixData, done: PeoafReady) {
        const peoaf = new Peoaf();

        for (const key in middlewareMatrix) {
            if (middlewareMatrix.hasOwnProperty(key)) {

                const middlewareMap: Array<SeoafMiddlewareFunc> = middlewareMatrix[key];
                const seoaf = new Seoaf();

                for (const key in middlewareMap) {
                    if (middlewareMap.hasOwnProperty(key)) {
                        const middleware: SeoafMiddlewareFunc = middlewareMap[key];
                        seoaf.use(middleware);
                    }
                }

                peoaf.use(seoaf.end.bind(seoaf));
            }
        }

        peoaf.end(done);
    }

    constructor(data: MiddlewareMatrixData) {
        this._data = data;
    }

    public ready(fn: PeoafReady): void {
        this._run(this._data, fn);
    }
}