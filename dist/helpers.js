"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Peoaf_1 = require("./Peoaf");
const Seoaf_1 = require("./Seoaf");
function runMiddleware(middlewareMatrix, done) {
    const peoaf = new Peoaf_1.Peoaf();
    for (const key in middlewareMatrix) {
        if (middlewareMatrix.hasOwnProperty(key)) {
            const middlewareMap = middlewareMatrix[key];
            const seoaf = new Seoaf_1.Seoaf();
            for (const key in middlewareMap) {
                if (middlewareMap.hasOwnProperty(key)) {
                    const middleware = middlewareMap[key];
                    seoaf.use(middleware);
                }
            }
            peoaf.use(seoaf.end.bind(seoaf));
        }
    }
    peoaf.end(done);
}
exports.runMiddleware = runMiddleware;
