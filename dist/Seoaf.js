"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Seoaf {
    constructor(fn, args) {
        this._middlewars = {};
        this._index = 0;
        this._length = 0;
        if (typeof fn === "function") {
            this.use(fn, args);
        }
    }
    static create(fn, args) {
        return new Seoaf(fn, args);
    }
    _getItem(mode = Seoaf.ITEM.current) {
        if (mode === Seoaf.ITEM.prev) {
            return this._middlewars[this._index - 1];
        }
        return this._middlewars[this._index];
    }
    _getPrevData() {
        const item = this._getItem(Seoaf.ITEM.prev);
        if (typeof item === "object") {
            return item.data;
        }
        return undefined;
    }
    _getArgs() {
        return this._middlewars[this._index].args;
    }
    _setDataAndDone(data) {
        this._getItem().data = data;
        this._getItem().isDone = true;
    }
    _next(data) {
        this._setDataAndDone(data);
        this._index++;
        if (this._index < this._length) {
            this._run();
        }
        else {
            this._ready(this._getPrevData());
        }
    }
    _run() {
        this._getItem().fn(this._next.bind(this), this._getPrevData(), this._getArgs());
    }
    use(fn, args) {
        if (!(args instanceof Array)) {
            args = [];
        }
        this._middlewars[this._index] = { fn, args, data: undefined, isDone: false };
        this._index++;
        return this;
    }
    end(fn) {
        this._ready = fn;
        this._length = this._index;
        this._index = 0;
        this._run();
        return this;
    }
}
Seoaf.ITEM = {
    current: 10,
    prev: 20
};
exports.Seoaf = Seoaf;
