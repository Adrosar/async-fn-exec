"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Peoaf_1 = require("./Peoaf");
const Seoaf_1 = require("./Seoaf");
class MiddlewareMatrix {
    constructor(data) {
        this._data = [];
        this._data = data;
    }
    static create(middlewareMatrix) {
        return new MiddlewareMatrix(middlewareMatrix);
    }
    _run(middlewareMatrix, done) {
        const peoaf = new Peoaf_1.Peoaf();
        for (const key in middlewareMatrix) {
            if (middlewareMatrix.hasOwnProperty(key)) {
                const middlewareMap = middlewareMatrix[key];
                const seoaf = new Seoaf_1.Seoaf();
                for (const key in middlewareMap) {
                    if (middlewareMap.hasOwnProperty(key)) {
                        const middleware = middlewareMap[key];
                        seoaf.use(middleware);
                    }
                }
                peoaf.use(seoaf.end.bind(seoaf));
            }
        }
        peoaf.end(done);
    }
    ready(fn) {
        this._run(this._data, fn);
    }
}
exports.MiddlewareMatrix = MiddlewareMatrix;
