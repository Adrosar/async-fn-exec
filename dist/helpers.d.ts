import { PeoafReady } from "./Peoaf";
import { SeoafMiddlewareFunc } from "./Seoaf";
export { SeoafNext } from "./Seoaf";
export declare function runMiddleware(middlewareMatrix: Array<Array<SeoafMiddlewareFunc>>, done: PeoafReady): void;
