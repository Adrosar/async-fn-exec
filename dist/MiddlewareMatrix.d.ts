import { PeoafReady } from "./Peoaf";
import { SeoafMiddlewareFunc } from "./Seoaf";
export { SeoafNext } from "./Seoaf";
export declare type MiddlewareMatrixData = Array<Array<SeoafMiddlewareFunc>>;
export declare class MiddlewareMatrix {
    static create(middlewareMatrix: MiddlewareMatrixData): MiddlewareMatrix;
    private _data;
    private _run(middlewareMatrix, done);
    constructor(data: MiddlewareMatrixData);
    ready(fn: PeoafReady): void;
}
