export declare type PeoafNext = {
    (data?: any): void;
};
export declare type PeoafMiddlewareFunc = {
    (next: PeoafNext, ...args: Array<any>): void;
};
export declare type PeoafReady = {
    (data: Array<any>): void;
};
export declare type PeoafMiddleware = {
    fn: PeoafMiddlewareFunc;
    args: Array<any>;
    data: any;
    isDone: boolean;
};
export declare type PeoafMiddlewareMap = {
    [name: number]: PeoafMiddleware;
};
export declare class Peoaf {
    static create(fn?: PeoafMiddlewareFunc): Peoaf;
    private _read;
    private _middlewares;
    private _index;
    private _check();
    private _getData();
    constructor(fn?: PeoafMiddlewareFunc);
    use(fn: PeoafMiddlewareFunc, args?: Array<any>): Peoaf;
    end(ready: PeoafReady): void;
}
