"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Peoaf {
    constructor(fn) {
        this._middlewares = {};
        this._index = 0;
        if (typeof fn === "function") {
            this.use(fn);
        }
    }
    static create(fn) {
        return new Peoaf(fn);
    }
    _check() {
        for (const key in this._middlewares) {
            if (this._middlewares.hasOwnProperty(key)) {
                if (this._middlewares[key].isDone === false) {
                    return false;
                }
            }
        }
        return true;
    }
    _getData() {
        let buffer = [];
        for (const key in this._middlewares) {
            if (this._middlewares.hasOwnProperty(key)) {
                buffer.push(this._middlewares[key].data);
            }
        }
        return buffer;
    }
    use(fn, args) {
        if (!(args instanceof Array)) {
            args = [];
        }
        this._middlewares[this._index] = { fn, args, data: undefined, isDone: false };
        this._index++;
        return this;
    }
    end(ready) {
        this._read = ready;
        for (const key in this._middlewares) {
            if (this._middlewares.hasOwnProperty(key)) {
                const item = this._middlewares[key];
                const next = (data) => {
                    item.isDone = true;
                    item.data = data;
                    if (this._check() === true) {
                        this._read(this._getData());
                    }
                };
                if (typeof item.fn === "function") {
                    item.fn(next, ...item.args);
                }
            }
        }
    }
}
exports.Peoaf = Peoaf;
