export declare type SeoafNext = {
    (data?: any): void;
};
export declare type SeoafReady = {
    (data: any): void;
};
export declare type SeoafMiddlewareFunc = {
    (next: SeoafNext, data: any, args: Array<any>): void;
};
export declare type SeoafMiddleware = {
    fn: SeoafMiddlewareFunc;
    args: Array<any>;
    data: any;
    isDone: boolean;
};
export declare type SeoafMiddlewareMap = {
    [name: number]: SeoafMiddleware;
};
export declare class Seoaf {
    static ITEM: {
        current: number;
        prev: number;
    };
    static create(fn?: SeoafMiddlewareFunc, args?: Array<any>): Seoaf;
    private _ready;
    private _middlewars;
    private _index;
    private _length;
    private _getItem(mode?);
    private _getPrevData();
    private _getArgs();
    private _setDataAndDone(data?);
    private _next(data?);
    private _run();
    constructor(fn?: SeoafMiddlewareFunc, args?: Array<any>);
    use(fn: SeoafMiddlewareFunc, args?: Array<any>): Seoaf;
    end(fn: SeoafReady): Seoaf;
}
